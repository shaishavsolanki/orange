//
//  Orange.swift
//  OrangeTreeGame
//
//  Created by MacStudent on 2019-02-20.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import Foundation
import SpriteKit

class Orange: SKSpriteNode {
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        
        
        let texture = SKTexture(imageNamed: "Orange")
        
        let size = texture.size()
        
        let color = UIColor.clear
        
        //call the super() function with the image
        super.init(texture: texture, color: color, size: size)
        
        // add physics to the orange
    
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width/2)
        
     //  self.physicsBody?.affectedByGravity = false
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
