//
//  GameScene.swift
//  OrangeTreeGame
//
//  Created by MacStudent on 2019-02-20.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import SpriteKit
import GameplayKit



class GameScene: SKScene {
    
    var orange:Orange?
    var statingPositionOfOrange:CGPoint = CGPoint(x: 0, y: 0)
    var lineNode = SKShapeNode()
    
    
    override func didMove(to view: SKView) {
        
        //add boundary around the scene
        
       // self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        
        
        //confuguration to draw a line
        
        self.lineNode.lineWidth = 20
        self.lineNode.lineCap = .round
        self.lineNode.strokeColor = UIColor.black
        addChild(lineNode)
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        //detect the touch
        
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        print("Finger Starting position is at: \(mouseLocation)")
        
        
        //detect what sprite was touch
        
        let spriteTouch = self.atPoint(mouseLocation)
        
        if(spriteTouch.name == "tree"){
           // print("Clicked on the tree")
            self.orange = Orange()
            orange?.position = mouseLocation
            addChild(orange!)
            
        self.orange?.physicsBody?.isDynamic = false
            
            //set the starting position of the finger
            self.statingPositionOfOrange = mouseLocation
            
            
        }
        else{
            //print("touched somewhere on screen: \(spriteTouch.name)")
        }
        //add orange when screen touched
        
       
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        print("Finger Ending position is at: \(mouseLocation)")
        
        let orangeEndingPosition = mouseLocation
        
        
        //get the difference between finger start and end
        let diffX = orangeEndingPosition.x - self.statingPositionOfOrange.x
        
        let diffY = orangeEndingPosition.y - self.statingPositionOfOrange.y
        
       
        
        //draw a line where orange is thrown
        let path = UIBezierPath()
        path.move(to: self.statingPositionOfOrange)
        path.addLine(to: mouseLocation)
        
        self.lineNode.path = path.cgPath
        
        
        
        // throw the orange based on that direction
        
        let direction = CGVector(dx: diffX, dy: diffY)
         self.orange?.physicsBody?.isDynamic = true
        self.orange?.physicsBody?.applyImpulse(direction)

      
 
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
     
        guard let touch = touches.first else{
            return
        }
        
        //get the location where user touched the screen
        let mouseLocation = touch.location(in: self)
        
        self.orange?.position = mouseLocation
        
       
    }
}
